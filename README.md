# Change this file to an introduction for your module!

Include links to the mdbook and the first set of slides for this topic.

# combined-template

- For mdbook, reference [here](https://gitlab.com/90cos/training/templates/combined-template)
- For Reveal JS Slides, reference [here](https://gitlab.com/90cos/training/templates/slides-template)

# Add multiple RevealJS SlideDecks

You are able to create multiple slide decks in the same mdbook. These slide decks are logically separated and do not interact with eachother. They can be access by adding a ```/slides``` at the end of your URL (depending on where you place them). There have been multiple slide deck added to this template to help you understand how to implement it yourself.

You will notice that there are separate slides located at different levels of the ```mdbook/src``` folder. They are listed below..
```sh
mdbook/src/cli/slides
mdbook/src/for_developers/slides
mdbook/src/format/slides
mdbook/src/slides
```
Each one of these ```slides``` folders will have an ```index.html``` file defining the slides content. To understand how to change this file to reflect your slide, check out the 'Reveal JS Slides, references' above.

You can place a copy of this ```slides``` folder anywhere within the ```mdbook/src``` folder and the gitlab pipeline will create your slides which can be accessed at the URL representative of the relative path.

# How To View and Work on MD Books locally(inside of VSCode) *PREFERRED METHOD*

0) Information to setup you local dev environment can be found (here)[https://gitlab.com/90cos/mttl/-/wikis/How-to's-(Developer)#how-to-setup-your-dev-environment]

1) Clone this repo.
2) Open the cloned repo in VSCode.
3) If you dont have the Remote Container extension installed, you can find instructions [here](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)
4) Click the button in the prompt asking you to "Reopen in container". If you are not prompted to open with a "Reopen in container" message, click the bottom left "open a remote window" button in green.
5) Fetch the template and unzip.
`curl -L https://gitlab.com/90COS/public/training/baselines/baseline-mdbook/-/jobs/artifacts/master/download?job=package_template --output public.zip && unzip public.zip`
6) Once the conatainer builds, navigate to the location of you MDBook using the VSCode terminal and build the mark down book using the mdbook utility.
`mdbook build -d public <path to your mdbook>`

**Note:** This path will have you src/ directory and book.toml

7) Serve the MDBook from the `<path to your mdbook>` for local hosting to view from your browser.
`mdbook serve -n 0.0.0.0 -p 3000`

**Note**: By using the `mdbook serve` command, this will show real time updates.

8) Open your prefered browser and enter `http://127.0.0.1:3000` to view your MDBook.

# How To View and Work on MD Books locally(outside of VSCode)

**If you prefer to not use VSCode remote-container, replace step 1 with the install of [Rust MDBook](https://github.com/rust-lang/mdBook#installation)**

1) Start a docker container that has all the tools:
`docker run -ti -p 3000:3000 -v $(pwd):/workspace registry.gitlab.com/90cos/docker-definitions/mdbook /bin/bash`
- Once in, change to your workspace directory. `cd /workspace`

**Note**: In your docker command, we are making a volume so you can work from your host and have the changes reflected  in the container

2) Clone the base lines.
`git clone git@gitlab.com:90cos/training/baselines/baseline-mdbook.git`
`git clone git@gitlab.com:90cos/training/baselines/baseline-slides.git`

3) Fetch the template and unzip.
`curl -L https://gitlab.com/90COS/public/training/baselines/baseline-mdbook/-/jobs/artifacts/master/download?job=package_template --output public.zip && unzip public.zip`

4) Build the Mark Down book using the mdbook utility.
`mdbook build -d public <path to your mdbook>`

**Note:** This path will have you src/ directory and book.toml

5) Serve the MDBook from the `<path to your mdbook>` for local hosting to view from your browser.
`mdbook serve -n 0.0.0.0 -p 3000`

**Note**: By using the `mdbook serve` command, this will show real time updates.

# README.mds in topic folders

To make a consistent structure every 'Topic' with subtopics (subjects) in the MDBook should be located in a topic folder with a README.md file used for the introduction of that topic. The README.md will be rendered in GitLab, Github, etc. and the existence of a README.md will guarantee the existence of a index.html file which will make mappings easier to maintain.

Example directory structure:
```
.
├── book.toml
└── src
    ├── README.md
    ├── **SUMMARY.md**
    ├── exam_setup
    │   ├── images
    │   │   ├── screengrab.jpg
    │   │   └── terminal.jpg
    │   └── perf_exam_env_setup.md
    ├── chapter 1
    │   ├── README.md
    │   │   page2.md
    │   │   page3.md
    ├── chapter 2
    │   ├── README.md
    │   │   page2.md
    │   │   page3.md
    ├── handouts
    │   ├── CCD-Basic
    │   │   ├── Handout-Perform-Basic-with-rubric.md
    │   │   └── Remote-Exam-Handout-with-rubric.md
    │   └── CCD-Senior
    │       ├── Handout-Perform-Senior-Linux.md
    │       └── Handout-Perform-Senior-Windows.md
    ├── lessons_learned
    │   ├── README.md
    │   └── ccd_exam_20200716-17
    │       └── ccd_exam_20200716-17.md
    └── slides
        ├── README.md
        ├── images
        │   ├── firstapp.png
        │   ├── igotrails.png
        │   ├── org_symbol.png
        │   ├── provisionpage.png
        │   └── resultspage.png
        └── index.html
```

6) Open your prefered browser and enter `http://127.0.0.1:3000` to view your MDBook.